We only use one Control structure, and keep mutating it each
iteration.  We do this instead of just returning a new Control each
time for 2 reasons:
 - To simplify code; if we don't have any changes to make, we can just
   leave a value alone, and it will stay.  This can simplify things
   with buttons.
 - To avoid putting unnecessary pressure on the GC.
