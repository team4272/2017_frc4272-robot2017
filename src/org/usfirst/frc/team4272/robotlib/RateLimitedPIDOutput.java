/**
 * Copyright (c) 2012 Precise Path Robotics, Inc
 * Copyright (c) 2017 Luke Shumaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Luke Shumaker <lukeshu@sbcglobal.net>
 */
package org.usfirst.frc.team4272.robotlib;

import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Timer;

/**
 * RateLimitedPIDOutput wraps a {@link PIDOutput}, unsuring that the
 * set value does not change too quickly.
 *
 * If the writer attempts to call {@link pidWrite(double)} with a
 * value that is too different than the previous value; it will
 * instead write a value that is closer to the previous value.
 */
public class RateLimitedPIDOutput implements PIDOutput {
	PIDOutput o;
	double m;
	double prev_rate = 0;
	double prev_time = 0;

	/**
	 * @param out The actual PIDOutput to write to.
	 * @param maxChange the maximum amount that the setpoint can
	 *        change by per second.
	 */
	public RateLimitedPIDOutput(PIDOutput out, double maxChange) {
		o = out;
		m = maxChange;
	}

	public void pidWrite(double rate) {
		double time = Timer.getFPGATimestamp();
		double dTime = time-prev_time;
		double dRate = rate-prev_rate;
		if (Math.abs(dRate/dTime)>m) {
			int sign = (dRate<0?-1:1);
			rate = prev_rate+(m*dTime*sign);
		}
		o.pidWrite(rate);
	}
}
