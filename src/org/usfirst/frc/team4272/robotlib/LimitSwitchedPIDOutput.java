/**
 * Copyright (c) 2015, 2017 Luke Shumaker.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the FIRST nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FIRST AND CONTRIBUTORS``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY NONINFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FIRST OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Luke Shumaker <lukeshu@sbcglobal.net>
 */
package org.usfirst.frc.team4272.robotlib;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.PIDOutput;

/**
 * LimitSwitchedPIDOutput wraps a {@link PIDOutput}, ensuring that it
 * does not go in a direction if a limit switch is tripped.
 */
public class LimitSwitchedPIDOutput implements PIDOutput {
	private final PIDOutput out;
	private final DigitalInput forward;
	private final DigitalInput bakward;
	private final boolean for_pressed;
	private final boolean bak_pressed;

	/**
	 * @param out The actual PIDOutput to write to.
	 * @param forward The forward (positive-direction) limit switch.
	 * @param for_pressed Which value from the forward switch is to be considered "pressed".
	 * @param backward The backward (negative-direction) limit switch.
	 * @param back_pressed Which value from the backward switch is to be considered "pressed".
	 */
	public LimitSwitchedPIDOutput(PIDOutput out,
			DigitalInput forward, boolean for_pressed,
			DigitalInput backward, boolean back_pressed) {
		this.out = out;
		this.forward = forward;
		this.bakward = backward;
		this.for_pressed = for_pressed;
		this.bak_pressed = back_pressed;
	}

	/**
	 * Like the full constructor, but assume that {@code true}
	 * indicates "pressed" for both switches.
	 *
	 * @param out The actual PIDOutput to write to.
	 * @param forward The forward (positive-direction) limit switch.
	 * @param backward The backward (negative-direction) limit switch.
	 */
	public LimitSwitchedPIDOutput(PIDOutput out, DigitalInput forward, DigitalInput backward) {
		this(out, forward, true, backward, true);
	}

	public void pidWrite(double v) {
		if (forward.get() == for_pressed) { v = Math.min(v, 0); }
		if (bakward.get() == bak_pressed) { v = Math.max(v, 0); }
		out.pidWrite(v);
	}
}
