/**
 * Copyright (c) 2016 Luke Shumaker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the FIRST nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FIRST AND CONTRIBUTORS``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY NONINFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FIRST OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Luke Shumaker <lukeshu@sbcglobal.net>
 */
package org.usfirst.frc.team4272.robotlib;

import edu.wpi.first.wpilibj.PIDOutput;

/**
 * A PIDOutput that turns a specified value into {@code NaN}.
 */
public class PIDNanny implements PIDOutput {
	private final PIDOutput out;
	private final double sentinel;

	/**
	 * Allocate a PIDNanny that wraps {@code out} and turns
	 * {@code sentinel} into {@link Double#NaN}.
	 *
	 * @param out The actual PIDOutput to write to.
	 * @param sentinel The value to replace with NaN.
	 */
	public PIDNanny(PIDOutput out, double sentinel) {
		this.out = out;
		this.sentinel = sentinel;
	}

	/**
	 * A convenience constructor with {@code sentinel} defaulting
	 * to {@code 0.0}.
	 *
	 * @param out The actual PIDOutput to write to.
	 */
	public PIDNanny(PIDOutput out) {
		this(out, 0.0);
	}

	public void pidWrite(double v) {
		if (v == sentinel) {
			v = Double.NaN;
		}
		out.pidWrite(v);
	}
}
