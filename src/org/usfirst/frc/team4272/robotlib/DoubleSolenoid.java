/**
 * Copyright (c) 2015, 2017 Luke Shumaker.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the FIRST nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FIRST AND CONTRIBUTORS``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY NONINFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FIRST OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Luke Shumaker <lukeshu@sbcglobal.net>
 */
package org.usfirst.frc.team4272.robotlib;

/**
 * DoubleSoleniod extends
 * {@link edu.wpi.first.wpilibj.DoubleSolenoid wpilibj.DoubleSoleniod},
 * adding the ability to disable it.
 *
 * Now, where things get useful is with enabling or disabling.  If
 * {@link #setEnabled(boolean) .setEnabled(false)}, then any calls to
 * {@link #set(Value) .set()} will be remembered, but ignored until
 * {@code .setEnabled(true)}.  As long as {@code .setEnabled(false)},
 * the actuated value will {@link Value.kOff}.
 */
public class DoubleSolenoid extends edu.wpi.first.wpilibj.DoubleSolenoid {
	private boolean enabled = true;
	private Value value;

	public DoubleSolenoid(int forwardChannel, int reverseChannel) {
		super(forwardChannel, reverseChannel);
		value = get();
	}
	public DoubleSolenoid(int moduleNumber, int forwardChannel, int reverseChannel) {
		super(moduleNumber, forwardChannel, reverseChannel);
		value = get();
	}

	/**
	 * Set whether the soleniod is enabled.  If the solenoid is
	 * not enabled, then the actuated state is {@link Value.kOff}.
	 * If {@link #set(Value)} or similar is call while disabled,
	 * the set will be remembered, but not take effect until it is
	 * re-enabled.
	 *
	 * @param enabled Whether the soleniod is enabled.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if (enabled) {
			set(value);
		} else {
			set(Value.kOff);
		}
	}

	/**
	 * A convenience wrapper around {@link #set(Value) .set()}
	 * because {@link Value.kForward} and {@link Value.kReverse}
	 * are awkward to deal with.
	 *
	 * @param forward Whether to {@code .set(Value.kForward)} or {@code kBackward}.
	 */
	public void setForward(boolean forward) {
		set(forward ? Value.kForward : Value.kReverse);
	}

	/**
	 * Returns whether or not it is set to forward.  If the
	 * solenoid is disabled, the return value is based on the
	 * remembered value that it will use upon being enabled.
	 *
	 * @return The current value of the solenoid.
	 */
	public boolean getForward() {
		return value == Value.kForward;
	}

	public void set(Value value) {
		this.value = value;
		if (enabled) {
			super.set(value);
		}
	}
}
