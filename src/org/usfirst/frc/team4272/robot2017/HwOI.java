// This file is not sufficiently creative to qualify for copyright.
package org.usfirst.frc.team4272.robot2017;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;

public class HwOI {
	public Joystick lStick = new Joystick(0);
	public Joystick rStick = new Joystick(1);
	public XboxController xbox = new XboxController(2);
}
