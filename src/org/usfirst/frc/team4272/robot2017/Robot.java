/**
 * Copyright (c) 2010-2017 Luke Shumaker.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the FIRST nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FIRST AND CONTRIBUTORS``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY NONINFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FIRST OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.usfirst.frc.team4272.robot2017;

import edu.wpi.first.wpilibj.IterativeRobot;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	private final HwRobot robot = new HwRobot();
	private final HwOI oi = new HwOI();
	private final Control control = new Control();

	private Autonomous auto;
	private Teleop teleop;

	private boolean networkInitialized = false;
	private void networkInit() {
		Autonomous.networkInit();
	}

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	public void robotInit() {
	}

	public void autonomousInit() {
		try {
			if (!networkInitialized) {
				networkInit();
				networkInitialized = true;
			}
			auto = new Autonomous(robot);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void autonomousPeriodic() {
		try {
			robot.run(auto.run(control));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void teleopInit() {
		try {
			if (!networkInitialized) {
				networkInit();
				networkInitialized = true;
			}
			teleop = new Teleop(robot);
		} catch (Exception e) {}
	}

	public void teleopPeriodic() {
		try {
			robot.run(teleop.run(control, oi));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void disabledInit() {
		try {
			if (!networkInitialized) {
				networkInit();
				networkInitialized = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void disabledPeriodic() {
	}

	public void testInit() {
		try {
			if (!networkInitialized) {
				networkInit();
				networkInitialized = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testPeriodic() {
	}
}
